#include "randseat.h"
#include<limits.h>
#include<string.h>
#include <time.h>
#include <stdlib.h>
/*무작위로 중복과 결번 없이 좌석 배치*/
void check_err(unsigned long long int *number, unsigned long long int people, unsigned long long int start)
{
	int rep, check = 0;
	for (rep = 0; rep < people; rep++)
	{
		outofloop:
		*(number + rep) = rand() % people + start;
		for (check = 0; check < rep; check++)
		{
			if (*(number + rep) == *(number + check))
			{
				while(1)
				{
					if(*(number + rep) != *(number + check))
						rep++;
						goto outofloop;
					*(number + rep) = rand() % people + start;
				} 
			}
		}
	}
}
/*인원출력*/
void printpeople(unsigned long long int *number, unsigned long long int people, FILE *f)
{	short x, length_of_num;
	char *string=malloc(sizeof(char)*21);
	unsigned long long int countnum, check = 0;
	for (countnum = 0; countnum < people; countnum++)
	{
		sprintf(string,"%llu", *(number + countnum));
		length_of_num=strlen(string);
		if (*(number + countnum) != 0)
		{
			printf("[%llu]", *(number + countnum));
			for(x=0;x<22-length_of_num;x++)
				printf(" ");
			fprintf(f, "[%llu]", *(number + countnum));
			for(x=0;x<22-length_of_num;x++)
				fprintf(f," ");
			check++;
			if (check % 2 == 0)
			{
				if (check % 8 == 0)
				{
					printf("\n");
					fprintf(f, "\n");
				}
				else
				{
					for(x=0;x<30;x++)
					{
						printf(" ");
						fprintf(f, " ");
					}
				}
			}
		}
	}
	fclose(f);
	free(string);
	printf("\n");
}
/*인원 확인 및 오류 점검*/
unsigned long long int check_missingnum(unsigned long long int missingno, long long start, unsigned long long int end)
{
	if (missingno == end - start)
	{
		puts("출력할 대상이 1명입니다.");
		return 0;
	}
	else if (missingno == 0)
	{
		puts("결번 없이 진행합니다.");
		return 1;
	}
	else if (missingno == end - start + 1)
	{
		puts("출력할 대상이 0명입니다.");
		return 0;
	}
	else if (missingno < 1)
	{
		puts("오류, 받지 않습니다!");
		return 0;
	}
}

/*결번처리함수*/
/*결번을 0으로 대체*/
void check_miss(unsigned long long int *number, unsigned long long int *miss_pointer, unsigned long long int people, unsigned long long int missingno)
{
	unsigned long long int countnum, countnum0, rep;
	for (countnum = 0; countnum < people; countnum++)
	{
		/*결번을 0으로 변환*/
		for (countnum0 = 0; countnum0 < missingno; countnum0++)
		{
			if (*(number + countnum) == *(miss_pointer + countnum0))
				*(number + countnum) = 0;
		}
	}
}
/* 시작 번호 입력 */
unsigned long long int firstnum()
{
	unsigned long long int start;
	printf("시작 번호:\n");
	scanf("%llu", &start);
	if (start <= 0)
	{
		printf("0번/1보다 작은 번호는 입력불가합니다.\n");
		return 0;
	}
	else
		return start;
}
/*끝 번호 입력*/
unsigned long long int lastnum(int start)
{
	unsigned long long int end;
	printf("끝 번호:\n");
	scanf("%llu", &end);
	if (end < start)
	{
		printf("0번/음수는 입력불가합니다.\n");
		return 0;
	}
	else
		return end;
}
/*결번 기입*/
unsigned long long int missing_num(unsigned long long int *miss_pointer, unsigned long long int missingno, unsigned long long int end)
{
	int misscount;
	for (misscount = 0; misscount < missingno; misscount++)
	{
		printf("결번:\n");
		scanf("%llu", miss_pointer + misscount);
		if ((*(miss_pointer + misscount) <= 0) |
			(*(miss_pointer + misscount) > end))
		{
			printf("0번/음수/끝번호보다 큰 번호는 입력불가입니다.\n");
			return 0;
		}
	}
}
