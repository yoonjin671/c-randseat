from tkinter import *
from win32api import GetSystemMetrics
f=open("result.txt", "rt")
x=f.read()
f.close()
x=x.split()
num=len(x)
root=Tk()
canvas=Canvas(root)
canvas.pack(side=LEFT,fill=Y,expand=True)
scrollbar=Scrollbar(root)
canvas.configure(yscrollcommand=scrollbar.set)
scrollbar.config(command=canvas.yview)
scrollbar.pack(side=LEFT,fill=Y)
col=0
frame_1=Frame(canvas)
frame_1.pack(side=LEFT,fill=BOTH,expand=True)
canvas.create_window((0, 0), window=frame_1, anchor='nw')
canvas.pack()
line_mark=0
row_=0
bigfont=("Helvetica", "20", 'bold')
for i in range(num):
	if col==12:
		col=0
		row_+=1
	txt=x[i]
	globals()['btn{}'.format(i)]=Button(frame_1, text="%s" % txt,bg="ivory", fg="brown" , activebackground='yellow', activeforeground="blue")
	globals()['btn{}'.format(i)].config(font=bigfont)
	globals()['btn{}'.format(i)].grid(row=row_,column=col,sticky='news')
	if(line_mark%2==1):
		globals()["line_lbl{}".format(i)]=Label(frame_1,text="     ")
		globals()["line_lbl{}".format(i)].config(font=bigfont)
		col+=1
		globals()["line_lbl{}".format(i)].grid(row=row_,column=col,sticky='news')
	col+=1
	line_mark+=1
frame_1.config(width=GetSystemMetrics(0)-30,height=GetSystemMetrics(1)*100000)
canvas.config(width=GetSystemMetrics(0)-30,height=GetSystemMetrics(1)*100000)
canvas.config(scrollregion=canvas.bbox("all"))
root.mainloop()
