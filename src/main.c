#include "randseat.h"
#include<stdlib.h>
#include<time.h>
int main()
{
	FILE *f;
	f = fopen("result.txt", "wt");
	srand(time(NULL));
	unsigned long long int start, end, people, missingno, missnumcheck, *miss_pointer;
	start = firstnum();
	if (start == 0)
		return 1;
	end = lastnum(start);
	if (end == 0)
		return 1;
	printf("결번의 수?\n");
	scanf("%lld", &missingno);
	miss_pointer = calloc(missingno, sizeof(unsigned long long int));
	missnumcheck = check_missingnum(missingno, start, end);
	if (missnumcheck == 0)
		return 1;

	missnumcheck = missing_num(miss_pointer, missingno, end);
	if (missnumcheck == 0)
		return 1;
	people = end - start + 1;
	unsigned long long int *number = calloc(people,sizeof(unsigned long long int));
	if (people < 0)
	{
		printf("음수를 입력하셨습니다!\n");
		return 1;
	}
	check_err(number, people, start);
	check_miss(number, miss_pointer, people, missingno);
	printpeople(number, people, f);
	free(number);
	free(miss_pointer);
	system("gui.exe");
	return 0;
}
