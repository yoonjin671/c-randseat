#ifndef RANDSEAT_H
#define RANDSEAT_H
#include<stdio.h>
/*인원 출력 함수*/
void printpeople(unsigned long long int *number, unsigned long long int people, FILE *f);
/*시작 번호 입력함수*/
unsigned long long int firstnum();
/*끝 번호 입력함수*/
unsigned long long int lastnum(int start);
/*결번처리 함수*/
void check_miss(unsigned long long int *number, unsigned long long int *miss_pointer, unsigned long long int people, unsigned long long int missingno);
unsigned long long int missing_num(unsigned long long int *miss_pointer, unsigned long long int missingno, unsigned long long int end);
void check_err(unsigned long long int *number, unsigned long long int people, unsigned long long int start);
unsigned long long int check_missingnum(unsigned long long int missingno, long long start, unsigned long long int end);
#endif
